<?php
include_once('Connection.php');

class Users extends Connection
{
    private $firstName;
    private $lastName;
    private $email;
    private $refID;
    private $amount;

    public function set($data = array()){
        if (array_key_exists('firstName', $data)) {
            $this->firstName = $data['firstName'];
        }
        if (array_key_exists('lastName', $data)) {
            $this->lastName = $data['lastName'];
        }
        if (array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists('amount', $data)) {
            $this->amount = $data['amount'];
        }
        if (array_key_exists('reference', $data)) {
            $this->refID = $data['reference'];
        }
//        echo $this->firstName;
//        echo $this->lastName;
//        echo $this->email;
//        echo $this->refID;
//        echo $this->amount;
//        var_dump($data);
//        var_dump($this->lastName);
//        return $this->postID;
    }

    public function store(){
        try{
            $stmt = $this->con->prepare("INSERT INTO `users` (`refID`, `email`, `firstName`, `lastName`, `amount`) VALUES (:refID, :email, :firstName, :lastName, :amount)");
            $result =  $stmt->execute(array(
                ':refID' => $this->refID,
                ':email' => $this->email,
                ':firstName' => $this->firstName,
                ':lastName' => $this->lastName,
                ':amount' => $this->amount
            ));

//            var_dump('this->postID');
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
            if($result){
                return true;
            }
        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function index($refID){
        try{
            $stmt = $this->con->prepare("SELECT * FROM `users` WHERE `refID` = '$refID'");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            var_dump($stmt->fetchAll(PDO::FETCH_ASSOC));
//            print_r($refID);
        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function totalStudents($startID, $endID){
        try{
            $stmt = $this->con->prepare("SELECT COUNT(DISTINCT `name`) AS `totalStudents` FROM `face` WHERE `id` >= '$startID' AND `id` <= '$endID'");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function studentsExpression($startID, $endID){
        try{
            $stmt = $this->con->prepare("SELECT `name`, `expression` FROM `face` WHERE `id` >= '$startID' AND `id` <= '$endID'");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function studentsName($startID, $endID){
        try{
            $stmt = $this->con->prepare("SELECT DISTINCT `name` FROM `face` WHERE `id` >= '$startID' AND `id` <= '$endID'");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

}