-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 24, 2020 at 05:30 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `payment-gateway`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `refID` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `firstName` varchar(100) NOT NULL,
  `lastName` varchar(100) NOT NULL,
  `amount` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `refID`, `email`, `firstName`, `lastName`, `amount`) VALUES
(10, 'TrxID675094783', 'noisyboy1020@gmail.com', 'MD', 'HOSSAIN', '1000'),
(12, 'TrxID208686364', 'noisyboy1020@gmail.com', 'MD', 'HOSSAIN', '1000'),
(13, 'TrxID440598630', 'noisyboy1020@gmail.com', 'MD', 'HOSSAIN', '1000'),
(15, 'TrxID914622947', 'noisyboy1020@gmail.com', 'MD', 'HOSSAIN', '1000'),
(32, 'TrxID81292590', 'noisyboy1020@gmail.com', 'MD', 'HOSSAIN', '1000'),
(54, 'TrxID508824389', 'noisyboy1020@gmail.com', 'MD', 'HOSSAIN', '1000'),
(56, 'TrxID68354428', 'noisyboy1020@gmail.com', 'MD', 'HOSSAIN', '1000'),
(57, 'TrxID323154258', 'noisyboy1020@gmail.com', 'MD', 'HOSSAIN', '1000'),
(58, 'TrxID41772030', 'noisyboy1020@gmail.com', 'MD', 'HOSSAIN', '1000'),
(59, 'TrxID634075124', 'noisyboy1020@gmail.com', 'MD', 'HOSSAIN', '1000'),
(60, 'TrxID109654477', 'noisyboy1020@gmail.com', 'MD', 'HOSSAIN', '1000'),
(61, 'TrxID979281665', 'noisyboy1020@gmail.com', 'MD', 'HOSSAIN', '1000'),
(62, 'TrxID686849854', 'noisyboy1020@gmail.com', 'MD', 'HOSSAIN', '1000'),
(63, 'TrxID590892236', 'noisyboy1020@gmail.com', 'MD', 'HOSSAIN', '1000'),
(64, 'TrxID242784462', 'noisyboy1020@gmail.com', 'Md.', 'Hossain', '1000'),
(65, 'TrxID746073756', 'noisyboy1020@gmail.com', 'Shadman', 'Shakib', '1000'),
(66, 'TrxID839724165', 'noisyboy1020@gmail.com', 'MD', 'HOSSAIN', '1000'),
(67, 'TrxID56678779', 'noisyboy1020@gmail.com', 'MD', 'HOSSAIN', '1000');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `refID` (`refID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
