<?php
include_once('../../src/Users.php');
session_start();
if (!empty($_SESSION) && isset($_GET['ref'])){
    $obj = new Users();
    $_SESSION['reference'] = $_GET['ref'];
    $data = filter_var_array($_SESSION, FILTER_SANITIZE_STRING);
    $obj->set($data);
    $output = $obj->store();
//    var_dump($output);

    if ($output == true)
        header("location: http://localhost:8080/payment/view/frontend/download.php");
    else
        header('location: http://localhost:8080/payment/');
//    echo 'if';
//    var_dump($data);
//    session_destroy();
}
//var_dump($_GET);
//var_dump($_SESSION);
else{
    header('location: http://localhost:8080/payment/');
//    echo 'else';
    session_destroy();
}