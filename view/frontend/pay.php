<?php
include_once ('../../include/header.php');
session_start();
if (!empty($_POST)){
    $data = filter_var_array($_POST, FILTER_SANITIZE_STRING);
    $firstName = $data['firstName'];
    $lastName = $data['lastName'];
    $email = $data['email'];
    $amount = $data['amount'];

    $_SESSION['firstName'] = $firstName;
    $_SESSION['lastName'] = $lastName;
    $_SESSION['email'] = $email;
    $_SESSION['amount'] = $amount;
}
else
    header('location: http://localhost:8080/payment/');
?>
    <main class="container">
        <section id="payment-gateway">
            <header>
                <h2>Hi, <?php echo $email?></h2>
                <p>Please confirm your payment.</p>
            </header>
            <button class="btn btn-info btn-block my-4" type="button" onclick="payWithPaystack()">Yes, confirmed!</button>
            <div class="form-submit">
                <a href="http://localhost:8080/payment/index.php?paymentcancel=yes"><button class="btn btn-info btn-block my-4" type="submit"> No, cancel please! </button></a>
            </div>
            <script>
                function payWithPaystack() {
                    const apiKey = 'pk_test_e7bb1a14eec55d010a2c33c89c4df3d03bd566c1'
                    var popup = PaystackPop.setup({
                        key: apiKey, // Replace with your public key
                        email: '<?php echo $email?>',
                        amount: <?php echo $amount?> * 100, // the amount value is multiplied by 100 to convert to the lowest currency unit
                        currency: 'NGN', // Use GHS for Ghana Cedis or USD for US Dollars
                        firstname: '<?php echo $firstName?>',
                        lastname: '<?php echo $lastName?>',
                        reference: 'TrxID'+Math.floor((Math.random() * 1000000000) + 1), // Generating a pseudo-unique reference
                        onClose: function() {
                            alert('Transaction was not completed, window closed.');
                            window.location.href = "http://localhost:8080/payment/"
                        },
                        callback: function(response){
                            alert('Transaction was successful. '+response.reference);
                            window.location.href = "http://localhost:8080/payment/view/backend/insert.php?ref="+ response.reference
                        },
                    });
                    popup.openIframe();
                }
            </script>
        </section>
    </main>

<?php include_once '../../include/footer.php';?>