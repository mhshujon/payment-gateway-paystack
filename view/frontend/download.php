<?php
include_once ('../../include/header.php');
session_start();
//var_dump($_SESSION);
if (!empty($_SESSION) || isset($_POST['trxid'])){
    if (!empty($_SESSION)){
        $firstName = $_SESSION['firstName'];
        $lastName = $_SESSION['lastName'];
        $email = $_SESSION['email'];
        $reference = $_SESSION['reference'];
        $amount = $_SESSION['amount'];
    }
    elseif (isset($_POST['trxid'])){
        include_once ('../../src/Users.php');
        $refID = $_POST['trxid'];
        $obj = new Users();
        $data = $obj->index($refID);

        if ($data != null){
            $reference = $data[0]['refID'];
            $firstName = $data[0]['firstName'];
            $lastName = $data[0]['lastName'];
            $email = $data[0]['email'];
            $amount = $data[0]['amount'];
        }
        else{
            $_POST['trxid'] = null;
            session_destroy();
            echo '<script>alert(\'Invalid TrxID\'); window.location.href = "http://localhost:8080/payment/"</script>';
        }

    }
}
else{
    header('location: http://localhost:8080/payment/');
    $_POST['trxid'] = null;
    session_destroy();
}
?>

<main class="container">
    <header>
        <h2>Thank you, noisyboy1020@gmail.com for choosing us.</h2>
        <h3>Please clink in the download link below to get your purchased product.</h3>
        <?php if(isset($_POST['trxid'])):?>
        <p>This page will automatically disappear after 30sec.</p>
        <?php endif;?>
        <?php if(!empty($_SESSION)):?>
        <p>Please don't close/refresh this tab unless you have already downloaded your product. This page will automatically disappear after 30sec.</p>
        <?php endif;?>
    </header>
    <section id="payment-verification">
        <table class="table table-striped">
            <tbody>
            <tr>
                <th scope="row">Name: </th>
                <td><?php echo $firstName.' '.$lastName?></td>
            </tr>
            <tr>
                <th scope="row">Email: </th>
                <td><?php echo $email?></td>
            </tr>
            <tr>
                <th scope="row">Reference: </th>
                <td><?php echo $reference?></td>
            </tr>
            <tr>
                <th scope="row">Amount Paid: </th>
                <td><?php echo 'NGN '.$amount?></td>
            </tr>
            <tr>
                <th scope="row">Download Link (Demo): </th>
                <td><a href="https://youtu.be/5vheNbQlsyU" target="_blank">https://youtu.be/5vheNbQlsyU</a></td>
            </tr>
            </tbody>
        </table>
    </section>
</main>

<script>
    setTimeout(function(){
        window.location.href = "http://localhost:8080/payment/"
        <?php $_POST['trxid'] = null; session_destroy();?>
    }, 30000);
</script>

<?php include_once '../../include/footer.php';?>
