<?php include_once 'include/header.php';
if (isset($_GET['paymentcancel'])){
    if ($_GET['paymentcancel'] == 'yes'){
        session_start();
        session_destroy();
        header('location: http://localhost:8080/payment/');
    }
}
?>

<section id="paymentForm" class="container">
    <!-- Default form login -->
    <form class="text-center border border-light p-5" action="view/frontend/pay.php" method="POST">
        <header>
            <h1 class="h4 mb-4">User Information</h1>
        </header>
        <div class="row">
            <div class="col-md-6">
                <!-- First Name -->
                <input type="text" id="firstname" class="form-control mb-4" name="firstName" placeholder="First Name" required>
            </div>
            <div class="col-md-6">
                <!-- Last Name -->
                <input type="text" id="lastname" class="form-control mb-4" name="lastName" placeholder="Last Name" required>
            </div>
            <div class="col-md-12">
                <!-- Email -->
                <input type="email" id="email" class="form-control mb-4" name="email" placeholder="Email" required>
                <input type="number" id="amount" class="form-control mb-4" name="amount" placeholder="Amount" value="1000" readonly>

                <div class="form-submit">
                    <button class="btn btn-info btn-block my-4" type="submit" onclick="payWithPaystack()"> Pay </button>
                </div>
            </div>
        </div>
    </form>
    <!-- Default form login -->
</section>

<section id="downloadForm" class="container">
    <form class="text-center border border-light p-5" action="view/frontend/download.php" method="POST">
        <header>
            <h1 class="h4 mb-4">Already Purchased?</h1>
        </header>
        <div class="row">
            <div class="col-md-12">
                <input type="text" id="trxid" class="form-control mb-4" name="trxid" placeholder="TrxID">

                <div class="form-submit">
                    <button class="btn btn-info btn-block my-4" type="submit">Download Now</button>
                </div>
            </div>
        </div>
    </form>
</section>

<?php include_once 'include/footer.php';?>